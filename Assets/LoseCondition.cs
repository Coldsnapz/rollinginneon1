﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.Rendering.PostProcessing;
using Lean.Pool;

public class LoseCondition : MonoBehaviour
{
    [SerializeField] PostProcessVolume volume = null;
    [SerializeField] Text StatusText = null;
    [SerializeField] GameObject DestroyedParticle = null;
    [SerializeField] GameObject PureCrystal = null;
    private ColorGrading colorGrading;
    Coroutine dead;
    private void Awake()
    {
        volume = GameObject.FindObjectOfType<PostProcessVolume>();
        volume.profile.TryGetSettings(out colorGrading);
        colorGrading.saturation.value = 0;

        colorGrading = GameObject.FindObjectOfType<ColorGrading>();
        if (StatusText == null)
        {
            StatusText = GameObject.Find("StatusText").GetComponent<Text>();
        }
        if (PureCrystal == null)
        {
            PureCrystal = GameObject.FindObjectOfType<WinCondition>().gameObject;
        }
    }

    private void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.tag == "Player")
        {
            if (!PureCrystal.GetComponent<WinCondition>().Winning)
            {
                GameObject.FindObjectOfType<GravityController>()._TopView = false;
                Instantiate(DestroyedParticle, other.transform.position, Quaternion.identity);
                PureCrystal.GetComponent<WinCondition>().textCountdown = 3;
                PureCrystal.GetComponent<WinCondition>().switchingText = 8;
                StatusText.enabled = true;
                StatusText.color = Color.white;
                StatusText.text = "You have been destroyed! We are sending you back to the last check point...";
                LeanPool.Despawn(other.gameObject);
                PureCrystal.GetComponent<WinCondition>().ReloadSceneWithDelay(3f);
            }
        }
    }
}
