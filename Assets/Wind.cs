﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class Wind : MonoBehaviour
{
    [SerializeField] float WindForce = 15;
    [SerializeField] float Range = 10;
    [SerializeField] ForceMode _forceMode = ForceMode.Force;
    void Update()
    {
        RaycastHit hit;
        if (Physics.Raycast(transform.position, transform.up, out hit, Range))
        {
            if (hit.collider.gameObject.tag == "Player")
            {
                Debug.DrawRay(transform.position, transform.up * hit.distance, Color.green);
                if (Application.isPlaying)
                {
                    hit.collider.gameObject.GetComponent<Rigidbody>().AddForce(transform.up * WindForce * Time.deltaTime * 100, _forceMode);
                }
            }
            else
            {
                Debug.DrawRay(transform.position, transform.up * hit.distance, Color.yellow);
            }
        }
        else
        {

            Debug.DrawRay(transform.position, transform.up * Range, Color.red);
        }
    }
}
